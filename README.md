# gcc for V16

This is a fork of [gcc](https://github.com/gcc-mirror/gcc) with support for [V16](https://gitlab.com/mbitsnbites/v16).

## Status

The V16 back end for gcc is still in early development. Currently you can only compile very simple C programs (no C++, no libc).

## Prerequisites

Ubuntu:
```bash
sudo apt install flex bison libbison-dev libgmp-dev libmpfr-dev libmpc-dev
```

You also have to have an installation of [V16 binutils](https://gitlab.com/mbitsnbites/binutils-v16) in your PATH.

## Building

### Build the bootstrap version of GCC

Configure and build:

```bash
$ mkdir build
$ cd build
$ ../configure --target=v16-elf --enable-languages=c --without-headers --with-newlib --with-gnu-as --with-gnu-ld
$ make all-gcc
```

Install:

```bash
$ sudo make install-gcc
```

## About this Git repo

The V16 back end for gcc is maintained as a branch that is periodically rebased on top of the latest upstream master branch and force pushed to the fork repository. To update your local clone you need to:

```bash
$ git fetch origin
$ git reset --hard origin/mbitsnbites/v16
```
