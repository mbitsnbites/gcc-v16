;; Constraint definitions for V16
;; Copyright (C) 2009-2024 Free Software Foundation, Inc.
;; Contributed by Marcus Geelnard <m@bitsnbites.eu>

;; This file is part of GCC.

;; GCC is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published
;; by the Free Software Foundation; either version 3, or (at your
;; option) any later version.

;; GCC is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;; License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GCC; see the file COPYING3.  If not see
;; <http://www.gnu.org/licenses/>.

;; -------------------------------------------------------------------------
;; Constraints
;; -------------------------------------------------------------------------

;; Off limits letters: E F V X g i m n o p r s

(define_memory_constraint "A"
  "An absolute address."
  (and (match_code "mem")
       (ior (match_test "GET_CODE (XEXP (op, 0)) == SYMBOL_REF")
	    (match_test "GET_CODE (XEXP (op, 0)) == LABEL_REF")
	    (match_test "GET_CODE (XEXP (op, 0)) == CONST"))))

(define_memory_constraint "S"
  "A symbol reference to a word-sized machine mode."
  (and (match_code "mem")
       (match_test "GET_CODE (XEXP (op, 0)) == SYMBOL_REF")
       (match_test "mode == SImode || mode == SFmode")))

(define_constraint "R"
  "A valid register based address."
  (and (match_code "mem")
       (match_test "v16_reg_based_mem_ref_p (op, reload_in_progress || reload_completed)")))

(define_constraint "I"
  "An unsigned 4-bit constant (0..15)"
  (and (match_code "const_int")
       (match_test "ival >= 0 && ival <= 15")))

(define_constraint "L"
  "A signed 4-bit constant (-8..7)"
  (and (match_code "const_int")
       (match_test "ival >= -8 && ival <= 7")))

(define_constraint "J"
  "A signed 8-bit constant (-128..127)"
  (and (match_code "const_int")
       (match_test "ival >= -128 && ival <= 127")))

(define_constraint "K"
  "A signed 9-bit(ish) constant (-256..-129, 128..254)"
  (and (match_code "const_int")
       (match_test "(ival >= -256 && ival <= -129) || (ival >= 128 && ival <= 254)")))

(define_constraint "h"
  "An unsigned 4-bit half-word offset (0, 2, 4, ..., 30)"
  (and (match_code "const_int")
       (match_test "ival >= 0 && ival <= 30 && (ival & 1) == 0")))

(define_constraint "w"
  "An unsigned 4-bit word offset (0, 4, 8, ..., 60)"
  (and (match_code "const_int")
       (match_test "ival >= 0 && ival <= 60 && (ival & 3) == 0")))

(define_constraint "P"
  "A signed 8-bit word-offset constant (-512, -508, ..., 508)"
  (and (match_code "const_int")
       (match_test "ival >= -512 && ival <= 508 && (ival & 3) == 0")))

(define_constraint "Q"
  "A signed 12-bit word-offset constant (-8192, -8188, ..., 8188)"
  (and (match_code "const_int")
       (match_test "ival >= -8192 && ival <= 8188 && (ival & 3) == 0")))

;; -------------------------------------------------------------------------
;; Register constraints.
;; -------------------------------------------------------------------------

;; Regular register-indirect calls can use most scalar registers.
(define_register_constraint "c" "CALL_REGS"
  "Registers which can hold a call address")

;; Sibcalls must not use any registers that may be clobbered by the
;; epilogue (e.g. callee-saved registers).
(define_register_constraint "j" "SIBCALL_REGS"
  "Registers which can hold a sibcall address")
