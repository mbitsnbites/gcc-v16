;; Predicate definitions for V16
;; Copyright (C) 2009-2024 Free Software Foundation, Inc.
;; Contributed by Marcus Geelnard <m@bitsnbites.eu>

;; This file is part of GCC.

;; GCC is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published
;; by the Free Software Foundation; either version 3, or (at your
;; option) any later version.

;; GCC is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;; License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GCC; see the file COPYING3.  If not see
;; <http://www.gnu.org/licenses/>.

;; -------------------------------------------------------------------------
;; Predicates
;; -------------------------------------------------------------------------

;; A valid generic move source operand.

(define_predicate "v16_movsrc_operand"
  (match_code "mem,reg,subreg,const_int,symbol_ref,const")
{
  if (MEM_P (op))
    return v16_valid_memsrc_operand (op);

  if (CONST_INT_P (op))
    return IN_RANGE (INTVAL (op), -128, 127);

  return general_operand (op, mode);
})

;; A valid generic move destination operand that does not require a register
;; clobbering operation.

(define_predicate "v16_movdst_operand"
  (match_code "mem,reg,subreg")
{
  if (MEM_P (op))
    return v16_reg_based_mem_ref_p (op, reload_in_progress || reload_completed);

  return general_operand (op, mode);
})

(define_predicate "v16_memconst_operand"
  (and (match_code "mem")
       (ior (match_test "GET_CODE (XEXP (op, 0)) == SYMBOL_REF")
	    (match_test "GET_CODE (XEXP (op, 0)) == LABEL_REF")
	    (match_test "GET_CODE (XEXP (op, 0)) == CONST"))))

;; Nonzero if OP can be an operand to an add instruction.

(define_predicate "v16_add_operand"
  (ior (match_code "reg")
       (and (match_code "const_int")
	    (match_test "IN_RANGE (INTVAL (op), -256, 254)"))))

;; Nonzero if OP can be an operand to a shift instruction.

(define_predicate "v16_reg_or_u4_operand"
  (ior (match_code "reg")
       (and (match_code "const_int")
	    (match_test "IN_RANGE (INTVAL (op), 0, 15)"))))

;; Nonzero if OP is a register or a short 4-bit signed integer.

(define_predicate "v16_reg_or_s4_operand"
  (ior (match_code "reg")
       (and (match_code "const_int")
	    (match_test "IN_RANGE (INTVAL (op), -8, 7)"))))

;; A valid comparison operator for CMP* instructions.

(define_predicate "v16_comparison_operator"
  (match_code "eq,lt,ltu"))

;; A valid operand for call instructions.

(define_predicate "v16_call_insn_operand"
  (ior (match_code "symbol_ref")
       (match_operand 0 "register_operand")))
