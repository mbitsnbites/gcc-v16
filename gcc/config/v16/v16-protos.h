/* Prototypes for v16.cc functions used in the md file & elsewhere.
   Copyright (C) 2009-2024 Free Software Foundation, Inc.
   Contributed by Marcus Geelnard <m@bitsnbites.eu>

   This file is part of GCC.

   GCC is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published
   by the Free Software Foundation; either version 3, or (at your
   option) any later version.

   GCC is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with GCC; see the file COPYING3.  If not see
   <http://www.gnu.org/licenses/>.  */

extern bool v16_can_load_as_shifted_s8 (rtx);
extern bool v16_int_to_shifted_s8 (int, int*, int*);
extern void v16_expand_prologue (void);
extern void v16_expand_epilogue (void);
extern int  v16_initial_elimination_offset (int, int);
extern bool v16_reg_based_mem_ref_p (rtx, bool);
extern bool v16_regno_ok_for_index_p (int, bool);
extern bool v16_regno_ok_for_base_p (int, bool);
extern bool v16_valid_memsrc_operand (rtx);
extern rtx v16_legitimize_call_address (rtx);
extern void v16_expand_conditional_branch (rtx *, machine_mode);
extern void v16_expand_conditional_store (rtx *, machine_mode);
extern int v16_label_align (rtx);