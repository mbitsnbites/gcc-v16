/* Target Code for V16
   Copyright (C) 2008-2024 Free Software Foundation, Inc.
   Contributed by Marcus Geelnard <m@bitsnbites.eu>

   This file is part of GCC.

   GCC is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published
   by the Free Software Foundation; either version 3, or (at your
   option) any later version.

   GCC is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with GCC; see the file COPYING3.  If not see
   <http://www.gnu.org/licenses/>.  */

#define IN_TARGET_CODE 1

#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "backend.h"
#include "target.h"
#include "rtl.h"
#include "tree.h"
#include "stringpool.h"
#include "attribs.h"
#include "df.h"
#include "regs.h"
#include "memmodel.h"
#include "emit-rtl.h"
#include "diagnostic-core.h"
#include "output.h"
#include "stor-layout.h"
#include "varasm.h"
#include "calls.h"
#include "expr.h"
#include "builtins.h"

#include "optabs.h"
#include "explow.h"
#include "cfgrtl.h"
#include "alias.h"

/* These 4 are needed to allow using constraints.  */
#include "insn-config.h"
#include "recog.h"
#include "tm_p.h"
#include "tm-constrs.h"

/* This file should be included last.  */
#include "target-def.h"

const enum reg_class v16_regno_to_class[FIRST_PSEUDO_REGISTER] = {
  GENERAL_REGS, GENERAL_REGS, GENERAL_REGS, GENERAL_REGS,  /*  r1  r2  r3  r4 */
  GENERAL_REGS, GENERAL_REGS, GENERAL_REGS, GENERAL_REGS,  /*  r5  r6  r7  r8 */
  GENERAL_REGS, GENERAL_REGS, GENERAL_REGS, GENERAL_REGS,  /*  r9 r10  lr  sp */
  VECTOR_REGS,  VECTOR_REGS,  VECTOR_REGS,  VECTOR_REGS,   /*  v1  v2  v3  v4 */
  GENERAL_REGS, GENERAL_REGS, T_REGS,       T_REGS,        /* ?fp ?ap   t  vt */
  T_REGS,       SPECIAL_REGS                               /*   l  vl         */
};

#define LOSE_AND_RETURN(msgid, x)		\
  do						\
    {						\
      v16_operand_lossage (msgid, x);		\
      return;					\
    } while (0)

#define BITSET_P(VALUE,BIT) (((VALUE) & (1L << (BIT))) != 0)

/* Worker function for TARGET_RETURN_IN_MEMORY.  */

static bool
v16_return_in_memory (const_tree type, const_tree fntype ATTRIBUTE_UNUSED)
{
  const HOST_WIDE_INT size = int_size_in_bytes (type);
  return (size == -1 || size > 2 * UNITS_PER_WORD);
}

/* Define how to find the value returned by a function.
   VALTYPE is the data type of the value (as a tree).
   If the precise function being called is known, FUNC is its
   FUNCTION_DECL; otherwise, FUNC is 0.

   We always return values in register r1 for V16.  */

static rtx
v16_function_value (const_tree valtype,
		   const_tree fntype_or_decl ATTRIBUTE_UNUSED,
		   bool outgoing ATTRIBUTE_UNUSED)
{
  return gen_rtx_REG (TYPE_MODE (valtype), V16_R1);
}

/* Define how to find the value returned by a library function.

   We always return values in register r1 for V16.  */

static rtx
v16_libcall_value (machine_mode mode,
		   const_rtx fun ATTRIBUTE_UNUSED)
{
  return gen_rtx_REG (mode, V16_R1);
}

/* Handle TARGET_FUNCTION_VALUE_REGNO_P.

   We always return values in register r1 for V16.  */

static bool
v16_function_value_regno_p (const unsigned int regno)
{
  return (regno == V16_R1);
}

/* Emit an error message when we're in an asm, and a fatal error for
   "normal" insns.  Formatted output isn't easily implemented, since we
   use output_operand_lossage to output the actual message and handle the
   categorization of the error.  */

static void
v16_operand_lossage (const char *msgid, rtx op)
{
  debug_rtx (op);
  output_operand_lossage ("%s", msgid);
}

/* The PRINT_OPERAND_ADDRESS worker.  */

static void
v16_print_operand_address (FILE *file, machine_mode, rtx x)
{
  if (REG_P (x))
    {
      /* Register indirect.  */
      asm_fprintf (file, "[%s, 0]", reg_names[REGNO (x)]);
    }

  else if (GET_CODE (x) == PLUS
	   && REG_P (XEXP (x, 0))
	   && CONST_INT_P (XEXP (x, 1)))
    {
      /* Register + offset.  */
      asm_fprintf (file, "[%s, %wd]",
		   reg_names[REGNO (XEXP (x, 0))],
		   INTVAL (XEXP (x, 1)));
    }

  else
    {
      /* This should be a plain symbol reference.  */
      output_addr_const (file, x);
    }
}

/* The PRINT_OPERAND worker.  */

static void
v16_print_operand (FILE *file, rtx op, int letter)
{
  enum rtx_code code;

  gcc_assert (op);
  code = GET_CODE (op);

  switch (letter)
    {
    case 0:
      /* Print an operand as without a modifier letter.  */
      switch (code)
	{
	case REG:
	  if (REGNO (op) > V16_V4)
	    internal_error ("internal error: bad register: %d", REGNO (op));
	  asm_fprintf (file, "%s", reg_names[REGNO (op)]);
	  return;

	case MEM:
	  output_address (GET_MODE (XEXP (op, 0)), XEXP (op, 0));
	  return;

	default:
	  /* No need to handle all strange variants, let output_addr_const
	     do it for us.  */
	  output_addr_const (file, op);
	  return;
	}
      break;

    case 'C':
      /* Conveniently, the V16 names for comparison conditions are the
	 same as their RTL equivalents.  */
      asm_fprintf (file, "%s", GET_RTX_NAME (code));
      break;

    default:
      LOSE_AND_RETURN ("invalid operand modifier letter", op);
    }
}

/* Information about a function's frame layout.  */

struct GTY(())  v16_frame_info {
  /* The size of the frame in bytes.  */
  HOST_WIDE_INT total_size;

  /* Bit X is set if the function saves or restores scalar register X.  */
  unsigned int mask;

  /* Offsets of register save areas from frame bottom */
  HOST_WIDE_INT sreg_sp_offset;

  /* Offset of virtual frame pointer from stack pointer/frame bottom */
  HOST_WIDE_INT frame_pointer_offset;

  /* Offset of hard frame pointer from stack pointer/frame bottom */
  HOST_WIDE_INT hard_frame_pointer_offset;

  /* The offset of arg_pointer_rtx from the bottom of the frame.  */
  HOST_WIDE_INT arg_pointer_offset;
};

/* Per-function machine data.  */

struct GTY(()) machine_function
{
  /* The current frame information, calculated by v16_compute_frame.  */
  struct v16_frame_info frame;
};

/* Zero initialization is OK for all current fields.  */

static struct machine_function *
v16_init_machine_status (void)
{
  return ggc_cleared_alloc<machine_function> ();
}


/* The TARGET_OPTION_OVERRIDE worker.  */
static void
v16_option_override (void)
{
  /* Set the per-function-data initializer.  */
  init_machine_status = v16_init_machine_status;
}

/* Try to convert an integer constant to a shifted S8.  */
bool
v16_int_to_shifted_s8 (int x, int* base, int* shift)
{
  int s;
  if (x >= -128 && x < 127)
    return false;  /* No need to shift.  */

  for (s = 1; s <= 15; ++s)
    {
      int b = x >> s;
      if ((b << s) != x)
	return false;
      if (b >= -128 && b <= 127)
	{
	  *base = b;
	  *shift = s;
	  return true;
	}
    }

  return false;
}

/* Check if an integer constant can be converted to to a shifted S8.  */
bool
v16_can_load_as_shifted_s8 (rtx op)
{
  int base;
  int shift;
  if (!CONST_INT_P (op))
    return false;
  return v16_int_to_shifted_s8 (INTVAL (op), &base, &shift);
}

/* Returns true if REGNO must be saved for the current function.  */

static bool
v16_callee_saved_regno_p (int regno)
{
  /* Check call-saved registers.  */
  if ((df_regs_ever_live_p (regno) && !call_used_or_fixed_reg_p (regno))
      || (crtl->calls_eh_return && (regno >= V16_EH_FIRST_REGNUM
				    && regno <= V16_EH_LAST_REGNUM)))
    {
      return true;
    }

  return false;
}

static unsigned int
v16_stack_align (unsigned int loc)
{
  return (loc + ((STACK_BOUNDARY/8)-1)) & -(STACK_BOUNDARY/8);
}

/* V16 stack frames grown downward.  High addresses are at the top.

	+-------------------------------+
	|                               |
	|  incoming stack arguments     |
	|                               |
	+-------------------------------+ <-- incoming stack pointer
	|                               |
	|  callee-allocated save area   |
	|  for arguments that are       |
	|  split between registers and  |
	|  the stack                    |
	|                               |
	+-------------------------------+ <-- arg_pointer_rtx
	|                               |
	|  callee-allocated save area   |
	|  for register varargs         |
	|                               |
	+-------------------------------+ <-- hard_frame_pointer_rtx;
	|                               |     stack_pointer_rtx + sreg_sp_offset
	|  Scalar registers save area   |       + UNITS_PER_WORD
	|                               |
	+-------------------------------+ <-- frame_pointer_rtx (virtual)
	|                               |
	|  local variables              |
	|                               |
      P +-------------------------------+
	|                               |
	|  outgoing stack arguments     |
	|                               |
	+-------------------------------+ <-- stack_pointer_rtx

   Dynamic stack allocations such as alloca insert data at point P.
   They decrease stack_pointer_rtx but leave frame_pointer_rtx and
   hard_frame_pointer_rtx unchanged.  */

/* If non-zero, this is an offset to be added to SP to redefine the CFA
   when restoring the FP register from the stack.  Only valid when generating
   the epilogue.  */

static int v16_epilogue_cfa_sp_offset;

/* Compute the size of the local area and the size to be adjusted by the
 * prologue and epilogue.  */

static void
v16_compute_frame (void)
{
  struct v16_frame_info *frame;
  HOST_WIDE_INT offset;
  unsigned int regno, num_saved = 0;

  frame = &cfun->machine->frame;

  memset (frame, 0, sizeof (*frame));

  /* Find out which scalar regs we need to save.  */
  num_saved = 0;
  for (regno = 0; regno <= 11; regno++)
    {
      if (v16_callee_saved_regno_p (regno))
	{
	  frame->mask |= 1 << regno;
	  ++num_saved;
	}
    }

  /* At the bottom of the frame are any outgoing stack arguments.  */
  offset = v16_stack_align (crtl->outgoing_args_size);

  /* Next are local stack variables.  */
  /* TODO(m): For some functions (e.g. __negdi2) we get an offset here even
     though there are no local stack variables.  */
  offset += v16_stack_align (get_frame_size ());

  /* The virtual frame pointer points above the local variables.  */
  frame->frame_pointer_offset = offset;

  /* Next are the callee-saved scalar regs.  */
  if (frame->mask)
    {
      offset += v16_stack_align (num_saved * UNITS_PER_WORD);
    }
  frame->sreg_sp_offset = offset - UNITS_PER_WORD;

  /* The hard frame pointer points above the callee-saved GPRs.  */
  frame->hard_frame_pointer_offset = offset;

  /* Next is the callee-allocated area for pretend stack arguments.  */
  offset += v16_stack_align (crtl->args.pretend_args_size);

  /* Arg pointer must be below pretend args, but must be above alignment
     padding.  */
  frame->arg_pointer_offset = offset - crtl->args.pretend_args_size;
  frame->total_size = offset;
}

static int
v16_num_saved_regs (struct v16_frame_info *frame)
{
  int num_saved = 0;
  for (int regno = 0; regno <= 11; regno++)
    if (BITSET_P (frame->mask, regno))
      ++num_saved;
  return num_saved;
}

/* Emit a move from SRC to DEST.  Assume that the move expanders can
   handle all moves if !can_create_pseudo_p ().  The distinction is
   important because, unlike emit_move_insn, the move expanders know
   how to force Pmode objects into the constant pool even when the
   constant pool address is not itself legitimate.  */

static rtx
v16_emit_move (rtx dest, rtx src)
{
  return (can_create_pseudo_p ()
	  ? emit_move_insn (dest, src)
	  : emit_move_insn_1 (dest, src));
}

/* Return a frame-related rtx that stores REG at MEM.
   REG must be a single register.  */

static rtx
v16_frame_set (rtx mem, rtx reg)
{
  rtx set = gen_rtx_SET (mem, reg);
  RTX_FRAME_RELATED_P (set) = 1;
  return set;
}

/* Make the last instruction frame-related and note that it performs
   the operation described by FRAME_PATTERN.  */

static void
v16_set_frame_expr (rtx frame_pattern)
{
  rtx insn;

  insn = get_last_insn ();
  RTX_FRAME_RELATED_P (insn) = 1;
  REG_NOTES (insn) = alloc_EXPR_LIST (REG_FRAME_RELATED_EXPR,
				      frame_pattern,
				      REG_NOTES (insn));
}

/* Save register REG to MEM.  Make the instruction frame-related.  */

static void
v16_save_reg (rtx reg, rtx mem)
{
  v16_emit_move (mem, reg);
  v16_set_frame_expr (v16_frame_set (mem, reg));
}

/* Restore register REG from MEM.  */

static void
v16_restore_reg (rtx reg, rtx mem)
{
  rtx insn = v16_emit_move (reg, mem);
  rtx dwarf = NULL_RTX;
  dwarf = alloc_reg_note (REG_CFA_RESTORE, reg, dwarf);

  if (v16_epilogue_cfa_sp_offset && REGNO (reg) == HARD_FRAME_POINTER_REGNUM)
    {
      rtx cfa_adjust_rtx = gen_rtx_PLUS (Pmode, stack_pointer_rtx,
					 GEN_INT (v16_epilogue_cfa_sp_offset));
      dwarf = alloc_reg_note (REG_CFA_DEF_CFA, cfa_adjust_rtx, dwarf);
    }

  REG_NOTES (insn) = dwarf;
  RTX_FRAME_RELATED_P (insn) = 1;
}

/* Add a constant offset of any distance to a frame related pointer
   (usually the stack pointer or the frame pointer). This may or may
   not clobber the R5 register.  */

static void
v16_emit_frame_addi (rtx dst_rtx, rtx src_rtx, const int offset)
{
  rtx insn;
  rtx frame_pattern;

  if (REGNO (dst_rtx) != REGNO (src_rtx))
    {
      insn = gen_movsi (dst_rtx, src_rtx);
      RTX_FRAME_RELATED_P (emit_insn (insn)) = 1;
    }

  if (offset >= -256 && offset <= 254)
    {
      insn = gen_addsi3 (dst_rtx, dst_rtx, GEN_INT (offset));
      RTX_FRAME_RELATED_P (emit_insn (insn)) = 1;
    }
  else
    {
      rtx tmpreg = gen_rtx_REG (Pmode, V16_R5);
      v16_emit_move (tmpreg, GEN_INT (offset));
      emit_insn (gen_addsi3 (dst_rtx, dst_rtx, tmpreg));

      /* Describe the effect of the previous instructions.  */
      frame_pattern = plus_constant (Pmode, src_rtx, offset);
      frame_pattern = gen_rtx_SET (dst_rtx, frame_pattern);
      v16_set_frame_expr (frame_pattern);
    }
}

/* For stack frames that can't be allocated with a single ADD immediate
   instruction, compute the best value to initially allocate. It must at
   a minimum allocate enough space to spill the callee-saved
   registers.  */

static HOST_WIDE_INT
v16_first_stack_step (struct v16_frame_info *frame)
{
  HOST_WIDE_INT size;

  /* Can we allocate the full stack frame with a single instruction?  */
  size = frame->total_size;
  if (size <= 128)
    return size;

  /* Next, try to allocate at least enough to store the callee-saved regs.  */
  size = v16_stack_align (frame->total_size - frame->sreg_sp_offset);
  if (size <= 128)
    return size;

  /* This can never happen - we will never store as many as 32 callee-saved
     regs.  */
  gcc_unreachable ();
}

void
v16_expand_prologue (void)
{
  struct v16_frame_info *frame = &cfun->machine->frame;
  HOST_WIDE_INT size = frame->total_size;
  int num_saved_regs;
  rtx insn;

  if (flag_stack_usage_info)
    current_function_static_stack_size = size;

  /* Save the registers.  */
  num_saved_regs = v16_num_saved_regs (frame);
  if (num_saved_regs > 0)
    {
      HOST_WIDE_INT step1 = v16_first_stack_step (frame);

      insn = gen_addsi3 (stack_pointer_rtx, stack_pointer_rtx, GEN_INT (-step1));
      RTX_FRAME_RELATED_P (emit_insn (insn)) = 1;
      size -= step1;

      /* Save the scalar registers. */
      HOST_WIDE_INT offset = frame->sreg_sp_offset - size;
      for (int regno = 11; regno >= 0; regno--)
	if (BITSET_P (frame->mask, regno))
	  {
	    rtx mem =
	      gen_frame_mem (word_mode, plus_constant (Pmode,
						       stack_pointer_rtx,
						       offset));
	    v16_save_reg (gen_rtx_REG (word_mode, regno), mem);
	    offset -= UNITS_PER_WORD;
	  }
    }

  /* Set up the frame pointer, if we're using one.  */
  if (frame_pointer_needed)
    {
      HOST_WIDE_INT offset = frame->hard_frame_pointer_offset - size;
      v16_emit_frame_addi (hard_frame_pointer_rtx, stack_pointer_rtx,
			   offset);
    }

  /* Allocate the rest of the frame.  */
  if (size > 0)
    {
      v16_emit_frame_addi (stack_pointer_rtx, stack_pointer_rtx, -size);
    }
}

void
v16_expand_epilogue (void)
{
  struct v16_frame_info *frame = &cfun->machine->frame;
  HOST_WIDE_INT step1 = frame->total_size;
  HOST_WIDE_INT step2 = 0;
  int num_saved_regs;

  /* Reset the epilogue cfa info before starting to emit the epilogue.  */
  v16_epilogue_cfa_sp_offset = 0;

  /* Move past any dynamic stack allocations.  */
  if (cfun->calls_alloca)
    {
      HOST_WIDE_INT offset = -frame->hard_frame_pointer_offset;
      v16_emit_frame_addi (stack_pointer_rtx, hard_frame_pointer_rtx,
			  offset);
    }

  /* If we need to restore registers, deallocate as much stack as
     possible in the second step without going out of range.  */
  num_saved_regs = v16_num_saved_regs (frame);
  if (num_saved_regs > 0)
    {
      step2 = v16_first_stack_step (frame);
      step1 -= step2;
    }

  /* Set TARGET to BASE + STEP1.  */
  if (step1 > 0)
    {
      v16_emit_frame_addi (stack_pointer_rtx, stack_pointer_rtx,
			  step1);
    }
  else if (frame_pointer_needed)
    {
      /* Tell v16_restore_reg to emit dwarf to redefine CFA when restoring
	 old value of FP.  */
      v16_epilogue_cfa_sp_offset = step2;
    }

  /* Restore the registers.  */
  num_saved_regs = v16_num_saved_regs (frame);
  if (num_saved_regs > 0)
    {
      HOST_WIDE_INT offset = frame->sreg_sp_offset -
			     (frame->total_size - step2) -
			     (num_saved_regs - 1) * UNITS_PER_WORD;
      for (int regno = 0; regno <= 11; regno++)
	if (BITSET_P (frame->mask, regno))
	  {
	    rtx mem =
	      gen_frame_mem (word_mode, plus_constant (Pmode,
						       stack_pointer_rtx,
						       offset));
	    v16_restore_reg (gen_rtx_REG (word_mode, regno), mem);
	    offset += UNITS_PER_WORD;
	  }
    }

  /* Deallocate the final bit of the frame.  */
  if (step2 > 0)
    {
      v16_emit_frame_addi (stack_pointer_rtx, stack_pointer_rtx,
			  step2);
    }

  /* If this function uses eh_return, add the final stack adjustment now.  */
  if (crtl->calls_eh_return)
    {
      emit_insn (gen_addsi3 (stack_pointer_rtx, stack_pointer_rtx, EH_RETURN_STACKADJ_RTX));
    }
}

/* Implements the macro INITIAL_ELIMINATION_OFFSET, return the OFFSET.  */

int
v16_initial_elimination_offset (int from, int to)
{
  HOST_WIDE_INT src, dest;

  v16_compute_frame ();

  if (to == HARD_FRAME_POINTER_REGNUM)
    dest = cfun->machine->frame.hard_frame_pointer_offset;
  else if (to == STACK_POINTER_REGNUM)
    dest = 0; /* The stack pointer is the base of all offsets, hence 0.  */
  else
    gcc_unreachable ();

  if (from == FRAME_POINTER_REGNUM)
    src = cfun->machine->frame.frame_pointer_offset;
  else if (from == ARG_POINTER_REGNUM)
    src = cfun->machine->frame.arg_pointer_offset;
  else
    gcc_unreachable ();

  return src - dest;
}

/* Worker function for TARGET_SETUP_INCOMING_VARARGS.  */

static void
v16_setup_incoming_varargs (cumulative_args_t cum_v,
			      const function_arg_info &,
			      int *pretend_size, int no_rtl)
{
  CUMULATIVE_ARGS *cum = get_cumulative_args (cum_v);
  int regno;
  int regs = 8 - *cum;

  *pretend_size = regs < 0 ? 0 : GET_MODE_SIZE (SImode) * regs;

  if (no_rtl)
    return;

  for (regno = *cum; regno < 8; regno++)
    {
      rtx reg = gen_rtx_REG (SImode, regno);
      rtx slot = gen_rtx_PLUS (Pmode,
			       gen_rtx_REG (SImode, ARG_POINTER_REGNUM),
			       GEN_INT (UNITS_PER_WORD * (3 + (regno-2))));

      emit_move_insn (gen_rtx_MEM (SImode, slot), reg);
    }
}


/* Return the fixed registers used for condition codes.  */

static bool
v16_fixed_condition_code_regs (unsigned int *p1, unsigned int *p2)
{
  *p1 = V16_T;
  *p2 = INVALID_REGNUM;
  return true;
}

/* Compare OPERANDS[1] with OPERANDS[2] using comparison in
   OPERAND[0] and jump to OPERANDS[3] if the condition holds.  */

static rtx_insn*
v16_emit_comparison (enum rtx_code code, rtx op1, rtx op2, machine_mode cmp_mode, bool* negate)
{
  rtx t_reg_rtx;
  bool reverse;

  /* We currently only support SI mode.  */
  if (cmp_mode != SImode)
    {
      gcc_unreachable ();
    }

  /* Canonicalize the comparison code.  */
  *negate = false;
  reverse = false;
  switch (code)
    {
    /* Directly supported (nothing to do)?  */
    case EQ:
    case LT:
    case LTU:
      break;

    /* Negate condition?  */
    case NE:
      code = EQ;
      *negate = true;
      break;
    case GE:
      code = LT;
      *negate = true;
      break;
    case GEU:
      code = LTU;
      *negate = true;
      break;

    /* Reverse operands?  */
    case GT:
      code = LT;
      reverse = true;
      break;
    case GTU:
      code = LTU;
      reverse = true;
      break;

    /* Negate condition & reverse operands?  */
    case LE:
      code = LT;
      *negate = true;
      reverse = true;
      break;
    case LEU:
      code = LTU;
      *negate = true;
      reverse = true;
      break;

    default:
      gcc_unreachable ();
    }

  /* Reverse operand order?  */
  if (reverse)
    {
      rtx tmp = op1;
      op1 = op2;
      op2 = tmp;
    }

  /* The left hand side comparison operand must be in a register.  */
  if (GET_CODE (op1) != REG)
    {
      /* Try to reverse the comparison operands.  */
      bool handled = false;
      if (CONST_INT_P (op1) && (GET_CODE (op2) == REG))
	{
	  reverse = false;
	  if (code == EQ)
	    {
	      /* This should probably never happen.  */
	      reverse = true;
	      handled = true;
	    }
	  else if (IN_RANGE (INTVAL (op1), 0, 14))
	    {
	      /* Transform A<B to !(B<=A), i.e. !(B<A+1) */
	      op1 = GEN_INT (INTVAL (op1) + 1);
	      *negate = !*negate;
	      reverse = true;
	      handled = true;
	    }

	  if (reverse)
	    {
	      rtx tmp = op1;
	      op1 = op2;
	      op2 = tmp;
	    }
	}

      /* If we couldn't handle it in a better way, force the first
	 operand into a register.  */
      if (!handled)
	op1 = force_reg (cmp_mode, op1);
    }

  /* Emit the comparison instruction.  */
  t_reg_rtx = gen_rtx_REG (SImode, V16_T);
  return emit_insn (gen_rtx_SET (t_reg_rtx, gen_rtx_fmt_ee (code, SImode, op1, op2)));
}

/* Compare OPERANDS[1] with OPERANDS[2] using comparison in
   OPERAND[0] and jump to OPERANDS[3] if the condition holds.  */

void
v16_expand_conditional_branch (rtx *operands, machine_mode cmp_mode)
{
  enum rtx_code code = GET_CODE (operands[0]);
  rtx op1 = operands[1];
  rtx op2 = operands[2];
  bool negate;

  /* Generate the comparison instruction.  */
  rtx_insn *insn = v16_emit_comparison (code, op1, op2, cmp_mode, &negate);

  /* TODO(m): Check the branch offset distance, and if necessary:
      - Negate the branch condition.
      - Inject an unconditional long branch instruction.
      - Use the conditional branch to skip the long branch.  */
  (void)insn;

  /* Emit the conditoinal branch instruction.  */
  if (negate)
    emit_jump_insn (gen_branch_false (operands[3]));
  else
    emit_jump_insn (gen_branch_true (operands[3]));
}

/* Compare OPERANDS[2] with OPERANDS[3] using comparison in
   OPERAND[1] and store 0 or 1 in OPERANDS[0] depending on the
   the outcome of the condition.  */

void
v16_expand_conditional_store (rtx *operands, machine_mode cmp_mode)
{
  enum rtx_code code = GET_CODE (operands[1]);
  rtx op1 = operands[2];
  rtx op2 = operands[3];
  bool negate;

  /* Generate the comparison instruction.  */
  rtx_insn *insn = v16_emit_comparison (code, op1, op2, cmp_mode, &negate);

  /* Emit the conditoinal store instruction.  */
  if (negate)
    emit_insn (gen_store_false (operands[0]));
  else
    emit_insn (gen_store_true (operands[0]));
}

/* Return the next register to be used to hold a function argument or
   NULL_RTX if there's no more space.  */

static rtx
v16_function_arg (cumulative_args_t cum_v, const function_arg_info &arg)
{
  CUMULATIVE_ARGS *cum = get_cumulative_args (cum_v);

  if (*cum <= V16_R4)
    return gen_rtx_REG (arg.mode, *cum);
  else
    return NULL_RTX;
}

#define V16_FUNCTION_ARG_SIZE(MODE, TYPE)	\
  ((MODE) != BLKmode ? GET_MODE_SIZE (MODE)	\
   : (unsigned) int_size_in_bytes (TYPE))

static void
v16_function_arg_advance (cumulative_args_t cum_v,
			  const function_arg_info &arg)
{
  CUMULATIVE_ARGS *cum = get_cumulative_args (cum_v);

  *cum = (*cum <= V16_R4
	  ? *cum + ((V16_FUNCTION_ARG_SIZE (arg.mode, arg.type) + 3) / 4)
	  : *cum);
}

/* Worker function for TARGET_FUNCTION_OK_FOR_SIBCALL.  */

static bool
v16_function_ok_for_sibcall (tree decl ATTRIBUTE_UNUSED,
			     tree exp ATTRIBUTE_UNUSED)
{
  /* Right now we allow sibcall for all call types. This may change in
     the future.  */
  return true;
}

/* Worker function for TARGET_WARN_FUNC_RETURN.  */

static bool
v16_warn_func_return (tree decl ATTRIBUTE_UNUSED)
{
  /* True if a function’s return statements should be checked for
     matching the function’s return type.
     Right now we don't suppress warnings.  */
  return true;
}

/* Return non-zero if the function argument described by ARG is to be
   passed by reference.  */

static bool
v16_pass_by_reference (cumulative_args_t, const function_arg_info &arg)
{
  if (arg.aggregate_type_p ())
    return true;
  unsigned HOST_WIDE_INT size = arg.type_size_in_bytes ();
  return size > 4*6;
}

/* Some function arguments will only partially fit in the registers
   that hold arguments.  Given a new arg, return the number of bytes
   that fit in argument passing registers.  */

static int
v16_arg_partial_bytes (cumulative_args_t cum_v, const function_arg_info &arg)
{
  CUMULATIVE_ARGS *cum = get_cumulative_args (cum_v);
  int bytes_left, size;

  if (*cum >= 8)
    return 0;

  if (v16_pass_by_reference (cum_v, arg))
    size = 4;
  else if (arg.type)
    {
      if (AGGREGATE_TYPE_P (arg.type))
	return 0;
      size = int_size_in_bytes (arg.type);
    }
  else
    size = GET_MODE_SIZE (arg.mode);

  bytes_left = (4 * 6) - ((*cum - 2) * 4);

  if (size > bytes_left)
    return bytes_left;
  else
    return 0;
}

/* Worker function for TARGET_STATIC_CHAIN.  */

static rtx
v16_static_chain (const_tree ARG_UNUSED (fndecl_or_type), bool incoming_p)
{
  rtx addr, mem;

  if (incoming_p)
    addr = plus_constant (Pmode, arg_pointer_rtx, 2 * UNITS_PER_WORD);
  else
    addr = plus_constant (Pmode, stack_pointer_rtx, -UNITS_PER_WORD);

  mem = gen_rtx_MEM (Pmode, addr);
  MEM_NOTRAP_P (mem) = 1;

  return mem;
}

/* Worker function for TARGET_ASM_TRAMPOLINE_TEMPLATE.  */

static void
v16_asm_trampoline_template (FILE *f)
{
  fprintf (f, "\tstw   r1, [sp, -4]\n");
  fprintf (f, "\tldi   r1, 0x0\n");
  fprintf (f, "\tstw   r1, [fp, 0x8]\n");
  fprintf (f, "\tldw   r1, [sp, -4]\n");
  fprintf (f, "\ttail  0x0\n");
}

/* Worker function for TARGET_TRAMPOLINE_INIT.  */

static void
v16_trampoline_init (rtx m_tramp, tree fndecl, rtx chain_value)
{
  rtx mem, fnaddr = XEXP (DECL_RTL (fndecl), 0);

  emit_block_move (m_tramp, assemble_trampoline_template (),
		   GEN_INT (TRAMPOLINE_SIZE), BLOCK_OP_NORMAL);

  mem = adjust_address (m_tramp, SImode, 4);
  emit_move_insn (mem, chain_value);
  mem = adjust_address (m_tramp, SImode, 16);
  emit_move_insn (mem, fnaddr);
}

/* Return true if register REGNO is a valid base register for mode MODE.
   STRICT_P is true if REG_OK_STRICT is in effect.  */

bool
v16_regno_ok_for_base_p (int regno, bool strict_p)
{
  if (!HARD_REGISTER_NUM_P (regno))
    {
      if (!strict_p)
	return true;

      if (!reg_renumber)
	return false;

      regno = reg_renumber[regno];
    }

  /* The fake registers will be eliminated to either the stack or
     hard frame pointer, both of which are usually valid base registers.
     Reload deals with the cases where the eliminated form isn't valid.  */
  return (SCALAR_GP_REGNUM_P (regno)
	  || regno == V16_SP
	  || regno == FRAME_POINTER_REGNUM
	  || regno == ARG_POINTER_REGNUM);
}

/* Helper function for `v16_legitimate_address_p'.  */

static bool
v16_reg_ok_for_base_p (const_rtx reg, bool strict_p)
{
  int regno = REGNO (reg);

  if (strict_p)
    return HARD_REGNO_OK_FOR_BASE_P (regno)
	   || HARD_REGNO_OK_FOR_BASE_P (reg_renumber[regno]);
  else
    return !HARD_REGISTER_NUM_P (regno)
	   || HARD_REGNO_OK_FOR_BASE_P (regno);
}

/* Return true for a valid register based memory addresses.  */

static bool
v16_reg_based_address_p (machine_mode mode, rtx x, bool strict_p)
{
  /* Register indirect.  */
  if (REG_P (x) && v16_reg_ok_for_base_p (x, strict_p))
    return true;

  /* Word-sized SP + imm8  */
  if (GET_MODE_SIZE (mode) == GET_MODE_SIZE (SImode)
      && GET_CODE (x) == PLUS
      && REG_P (XEXP (x, 0))
      && v16_reg_ok_for_base_p (XEXP (x, 0), strict_p)
      && REGNO (XEXP (x, 0)) == V16_SP
      && CONST_INT_P (XEXP (x, 1))
      && IN_RANGE (INTVAL (XEXP (x, 1)), -128, 127))
    return true;

  /* Generic word-sized reg + imm4  */
  if (GET_MODE_SIZE (mode) == GET_MODE_SIZE (SImode)
      && GET_CODE (x) == PLUS
      && REG_P (XEXP (x, 0))
      && v16_reg_ok_for_base_p (XEXP (x, 0), strict_p)
      && CONST_INT_P (XEXP (x, 1))
      && IN_RANGE (INTVAL (XEXP (x, 1)), 0, 60))
    return true;

  /* Generic half-word-sized reg + imm4  */
  if (GET_MODE_SIZE (mode) == GET_MODE_SIZE (HImode)
      && GET_CODE (x) == PLUS
      && REG_P (XEXP (x, 0))
      && v16_reg_ok_for_base_p (XEXP (x, 0), strict_p)
      && CONST_INT_P (XEXP (x, 1))
      && IN_RANGE (INTVAL (XEXP (x, 1)), 0, 30))
    return true;

  /* Generic byte-sized reg + imm4  */
  if (GET_MODE_SIZE (mode) == GET_MODE_SIZE (QImode)
      && GET_CODE (x) == PLUS
      && REG_P (XEXP (x, 0))
      && v16_reg_ok_for_base_p (XEXP (x, 0), strict_p)
      && CONST_INT_P (XEXP (x, 1))
      && IN_RANGE (INTVAL (XEXP (x, 1)), 0, 15))
    return true;

  return false;
}

/* Return true for register based memory references.  */

bool
v16_reg_based_mem_ref_p (rtx x, bool strict_p)
{
  return MEM_P (x) &&
	 v16_reg_based_address_p (GET_MODE (x), XEXP (x, 0), strict_p);
}

/* Implement TARGET_CANNOT_FORCE_CONST_MEM.
   Determine if it's legal to put X into the constant pool.  */

static bool
v16_cannot_force_const_mem (machine_mode mode ATTRIBUTE_UNUSED,
			     rtx x)
{
  /* TODO(m): Don't allow this for TLS references.  */
  return false;
}


/* Implement TARGET_LEGITIMATE_CONSTANT_P.  Returns nonzero if the
   constant value X is a legitimate general operand.
   It is given that X satisfies CONSTANT_P or is a CONST_DOUBLE.  */

static bool
v16_legitimate_constant_p (machine_mode mode, rtx x)
{
  return CONSTANT_P (x);
}

/* Worker function for TARGET_LEGITIMATE_ADDRESS_P.  */

static bool
v16_legitimate_address_p (machine_mode mode,
			 rtx x, bool strict_p,
			 code_helper = ERROR_MARK)
{
  /* A symbol reference: Word-sized PC + imm8  */
  if (SYMBOL_REF_P (x) && GET_MODE_SIZE (mode) == GET_MODE_SIZE (SImode))
    return true;

  /* Register based address?  */
  if (v16_reg_based_address_p (mode, x, strict_p))
    return true;

  return false;
}

/* Used by the v16_movsrc_operand predicates.  */

bool
v16_valid_memsrc_operand (rtx op)
{
  if (MEM_P (op))
    {
      machine_mode mode = GET_MODE (op);
      rtx x = XEXP (op, 0);
      bool strict = reload_in_progress || reload_completed;
      return v16_legitimate_address_p (mode, x, strict);
    }

  return false;
}

/* Make ADDR suitable for use as a call or sibcall target.  */

rtx
v16_legitimize_call_address (rtx addr)
{
  if (!v16_call_insn_operand (addr, VOIDmode))
    {
      return copy_addr_to_reg (addr);
    }
  return addr;
}

/* Implement LABEL_ALIGN. Some V16 branch targets need to be aligned to a
   4-byte boundary.  */

int
v16_label_align (rtx label)
{
  /* TODO(m); Use the information gathered in v16_reorg (TBD) to only force
     4-byte alignment for labels that are targets for certain branch
     instructions (see nios2). Also honor the user's selection in
     ALIGN_LABELS (-falign-labels).  */
#if 0
  int n = CODE_LABEL_NUMBER (label);

  if (label_align && n >= min_labelno && n <= max_labelno)
    return MAX (label_align[n - min_labelno], align_labels.levels[0].log);
  return align_labels.levels[0].log;
#else
  /* Hack: Force 4-byte alignment for all labels.  */
  (void)label;
  return 2;
#endif
}

/* Initialize the GCC target structure.  */

#undef  TARGET_PROMOTE_PROTOTYPES
#define TARGET_PROMOTE_PROTOTYPES	hook_bool_const_tree_true

#undef  TARGET_RETURN_IN_MEMORY
#define TARGET_RETURN_IN_MEMORY		v16_return_in_memory
#undef  TARGET_MUST_PASS_IN_STACK
#define TARGET_MUST_PASS_IN_STACK	must_pass_in_stack_var_size
#undef  TARGET_PASS_BY_REFERENCE
#define TARGET_PASS_BY_REFERENCE        v16_pass_by_reference
#undef  TARGET_ARG_PARTIAL_BYTES
#define TARGET_ARG_PARTIAL_BYTES        v16_arg_partial_bytes
#undef  TARGET_FUNCTION_ARG
#define TARGET_FUNCTION_ARG		v16_function_arg
#undef  TARGET_FUNCTION_ARG_ADVANCE
#define TARGET_FUNCTION_ARG_ADVANCE	v16_function_arg_advance
#undef  TARGET_FUNCTION_OK_FOR_SIBCALL
#define TARGET_FUNCTION_OK_FOR_SIBCALL	v16_function_ok_for_sibcall
#undef  TARGET_WARN_FUNC_RETURN
#define TARGET_WARN_FUNC_RETURN		v16_warn_func_return

#undef TARGET_CANNOT_FORCE_CONST_MEM
#define TARGET_CANNOT_FORCE_CONST_MEM	v16_cannot_force_const_mem

#undef TARGET_LEGITIMATE_CONSTANT_P
#define TARGET_LEGITIMATE_CONSTANT_P	v16_legitimate_constant_p

#undef  TARGET_LEGITIMATE_ADDRESS_P
#define TARGET_LEGITIMATE_ADDRESS_P	v16_legitimate_address_p

#undef  TARGET_SETUP_INCOMING_VARARGS
#define TARGET_SETUP_INCOMING_VARARGS 	v16_setup_incoming_varargs

#undef	TARGET_FIXED_CONDITION_CODE_REGS
#define	TARGET_FIXED_CONDITION_CODE_REGS v16_fixed_condition_code_regs

/* Define this to return an RTX representing the place where a
   function returns or receives a value of data type RET_TYPE, a tree
   node representing a data type.  */
#undef TARGET_FUNCTION_VALUE
#define TARGET_FUNCTION_VALUE v16_function_value
#undef TARGET_LIBCALL_VALUE
#define TARGET_LIBCALL_VALUE v16_libcall_value
#undef TARGET_FUNCTION_VALUE_REGNO_P
#define TARGET_FUNCTION_VALUE_REGNO_P v16_function_value_regno_p

#undef TARGET_STATIC_CHAIN
#define TARGET_STATIC_CHAIN v16_static_chain
#undef TARGET_ASM_TRAMPOLINE_TEMPLATE
#define TARGET_ASM_TRAMPOLINE_TEMPLATE v16_asm_trampoline_template
#undef TARGET_TRAMPOLINE_INIT
#define TARGET_TRAMPOLINE_INIT v16_trampoline_init

#undef TARGET_OPTION_OVERRIDE
#define TARGET_OPTION_OVERRIDE v16_option_override

#undef  TARGET_PRINT_OPERAND
#define TARGET_PRINT_OPERAND v16_print_operand
#undef  TARGET_PRINT_OPERAND_ADDRESS
#define TARGET_PRINT_OPERAND_ADDRESS v16_print_operand_address

#undef  TARGET_CONSTANT_ALIGNMENT
#define TARGET_CONSTANT_ALIGNMENT constant_alignment_word_strings

/* The Global `targetm' Variable.  */

struct gcc_target targetm = TARGET_INITIALIZER;

#include "gt-v16.h"
