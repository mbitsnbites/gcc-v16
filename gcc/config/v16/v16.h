/* Target Definitions for V16
   Copyright (C) 2008-2024 Free Software Foundation, Inc.
   Contributed by Marcus Geelnard <m@bitsnbites.eu>

   This file is part of GCC.

   GCC is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published
   by the Free Software Foundation; either version 3, or (at your
   option) any later version.

   GCC is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with GCC; see the file COPYING3.  If not see
   <http://www.gnu.org/licenses/>.  */

#include "config/v16/v16-opts.h"

#ifndef GCC_V16_H
#define GCC_V16_H

#undef  STARTFILE_SPEC
#define STARTFILE_SPEC					\
	" %{!mno-ctor-dtor:%{!mno-crt0:crt0%O%s}}"	\
	" %{mno-ctor-dtor:%{!mno-crt0:crt1%O%s}}"	\
	" %{!mno-ctor-dtor:crti.o%s}"			\
	" %{!mno-ctor-dtor:crtbegin.o%s}"

#undef  ENDFILE_SPEC
#define ENDFILE_SPEC					\
	" %{!mno-ctor-dtor:crtend.o%s}"			\
	" %{!mno-ctor-dtor:crtn.o%s}"

#undef  LIB_SPEC
#define LIB_SPEC					\
	" --start-group"				\
	" -lc"						\
	" %{msim:-lv16sim}"				\
	" %{!msim:-lv16default}"			\
	" --end-group"

#define TARGET_DEFAULT (MASK_MUL)

/* Layout of Source Language Data Types */

#define INT_TYPE_SIZE 32
#define SHORT_TYPE_SIZE 16
#define LONG_TYPE_SIZE 32
#define LONG_LONG_TYPE_SIZE 64

#define DEFAULT_SIGNED_CHAR 0

#undef  SIZE_TYPE
#define SIZE_TYPE "unsigned int"

#undef  PTRDIFF_TYPE
#define PTRDIFF_TYPE "int"

#undef  WCHAR_TYPE
#define WCHAR_TYPE "unsigned int"

#undef  WCHAR_TYPE_SIZE
#define WCHAR_TYPE_SIZE BITS_PER_WORD

/* V16 registers:

 Scalar registers:
   R1  - general purpose 32-bit register
   R2  - general purpose 32-bit register
   R3  - general purpose 32-bit register
   R4  - general purpose 32-bit register
   R5  - general purpose 32-bit register
   R6  - general purpose 32-bit register
   R7  - general purpose 32-bit register
   R8  - general purpose 32-bit register
   R9  - general purpose 32-bit register
   R10 - general purpose 32-bit register (also FP)
   LR  - link register (R11)
   SP  - stack pointer (R12)

 Vector registers:
   V1  - general purpose vector register (N*32 bits)
   V2  - general purpose vector register (N*32 bits)
   V3  - general purpose vector register (N*32 bits)
   V4  - general purpose vector register (N*32 bits)

 T registers (branch conditions):
   T   - result of scalar comparison (true/false)
   VT  - result of vector comparison (N * true/false)
   L   - result of setvl (continue/break loop)

 Special registers:
   VL  - vector length register
*/

#define REGISTER_NAMES {	\
  "r1", "r2", "r3", "r4",	\
  "r5", "r6", "r7", "r8",	\
  "r9", "r10", "lr", "sp",	\
  "v1", "v2", "v3", "v4", 	\
  "t", "vt", "l", "vl",		\
  "?fp", "?ap" }

#define V16_R1     0
#define V16_R2     1
#define V16_R3     2
#define V16_R4     3
#define V16_R5     4
#define V16_R6     5
#define V16_R7     6
#define V16_R8     7
#define V16_R9     8
#define V16_R10    9
#define V16_LR     10
#define V16_SP     11

#define V16_V1     12
#define V16_V2     13
#define V16_V3     14
#define V16_V4     15

#define V16_T      16
#define V16_VT     17
#define V16_L      18
#define V16_VL     19
#define V16_QFP    20
#define V16_QAP    21

#define FIRST_PSEUDO_REGISTER 22

enum reg_class
{
  NO_REGS,
  SIBCALL_REGS,
  CALL_REGS,
  GENERAL_REGS,
  VECTOR_REGS,
  T_REGS,
  SPECIAL_REGS,
  ALL_REGS,
  LIM_REG_CLASSES
};


#define REG_CLASS_CONTENTS \
{ { 0x00000000 }, /* NO_REGS:      Empty                  */	\
  { 0x0000000F }, /* SIBCALL_REGS: r1 - r4                */	\
  { 0x00000FFF }, /* CALL_REGS:    r1 - sp                */	\
  { 0x00300FFF }, /* GENERAL_REGS: r1 - sp, ?fp, ?ap      */	\
  { 0x0000F000 }, /* VECTOR_REGS:  v1 - v4                */	\
  { 0x00070000 }, /* T_REGS:       t, vt, l               */	\
  { 0x000F0000 }, /* SPECIAL_REGS: t, vt, l, vl           */	\
  { 0x003FFFFF }  /* ALL_REGS:     All registers          */	\
}

#define N_REG_CLASSES LIM_REG_CLASSES

#define REG_CLASS_NAMES {\
    "NO_REGS", \
    "SIBCALL_REGS", \
    "CALL_REGS", \
    "GENERAL_REGS", \
    "VECTOR_REGS", \
    "T_REGS", \
    "SPECIAL_REGS", \
    "ALL_REGS" }

/* TODO(m): v1-v4 are not fixed, but we mark them as such just to keep
   the register allocator away from them.  */

#define FIXED_REGISTERS {			\
/*  r1,  r2,  r3,  r4 */	0, 0, 0, 0,	\
/*  r5,  r6,  r7,  r8 */	0, 0, 0, 0,	\
/*  r9, r10,  lr,  sp */	0, 0, 0, 1,	\
/*  v1,  v2,  v3,  v4 */	1, 1, 1, 1,	\
/*   t,  vt    l,  vl */	1, 1, 1, 1,	\
/* ?fp, ?ap           */	1, 1		\
}

#define CALL_REALLY_USED_REGISTERS {		\
/*  r1,  r2,  r3,  r4 */	1, 1, 1, 1,	\
/*  r5,  r6,  r7,  r8 */	1, 0, 0, 0,	\
/*  r9, r10,  lr,  sp */	0, 0, 0, 0,	\
/*  v1,  v2,  v3,  v4 */	1, 1, 1, 1,	\
/*   t,  vt    l,  vl */	1, 1, 1, 1,	\
/* ?fp, ?ap           */	1, 1		\
}

/* We can't copy to or from our CC register. */
#define AVOID_CCMODE_COPIES 1

/* A C expression whose value is a register class containing hard
   register REGNO.  */
extern const enum reg_class v16_regno_to_class[];
#define REGNO_REG_CLASS(REGNO) v16_regno_to_class[(REGNO)]

/* Non-zero if REGNO is a scalar general purpose register.  */
#define SCALAR_GP_REGNUM_P(REGNO)						\
  (((unsigned) (REGNO - V16_R1)) <= (V16_SP - V16_R1))

/* The Overall Framework of an Assembler File */

#undef  ASM_SPEC
#define ASM_COMMENT_START ";"
#define ASM_APP_ON ""
#define ASM_APP_OFF ""

#define FILE_ASM_OP     "\t.file\n"

/* Switch to the text or data segment.  */
#define TEXT_SECTION_ASM_OP  "\t.text"
#define DATA_SECTION_ASM_OP  "\t.data"

/* Assembler Commands for Alignment */

#define ASM_OUTPUT_ALIGN(STREAM,POWER) \
	fprintf (STREAM, "\t.p2align\t%d\n", POWER);

/* Output and Generation of Labels */

#define GLOBAL_ASM_OP "\t.global\t"

/* Passing Arguments in Registers */

/* A C type for declaring a variable that is used as the first
   argument of `FUNCTION_ARG' and other related values.  */
#define CUMULATIVE_ARGS unsigned int

/* If defined, the maximum amount of space required for outgoing arguments
   will be computed and placed into the variable
   `current_function_outgoing_args_size'.  No space will be pushed
   onto the stack for each call; instead, the function prologue should
   increase the stack frame size by this amount.  */
#define ACCUMULATE_OUTGOING_ARGS 1

/* A C statement (sans semicolon) for initializing the variable CUM
   for the state at the beginning of the argument list.
   For V16, the first arg is passed in register 0 (aka r1).  */
#define INIT_CUMULATIVE_ARGS(CUM,FNTYPE,LIBNAME,FNDECL,N_NAMED_ARGS) \
  (CUM = V16_R1)

/* How Scalar Function Values Are Returned */

/* STACK AND CALLING */

/* Define this macro if pushing a word onto the stack moves the stack
   pointer to a smaller address.  */
#define STACK_GROWS_DOWNWARD 1

/* Define this if the above stack space is to be considered part of the
   space allocated by the caller.  */
#define OUTGOING_REG_PARM_STACK_SPACE(FNTYPE) 1

/* Define this if it is the responsibility of the caller to allocate
   the area reserved for arguments passed in registers.  */
#define REG_PARM_STACK_SPACE(FNDECL) 0

/* Offset from the argument pointer register to the first argument's
   address.  On some machines it may depend on the data type of the
   function.  */
#define FIRST_PARM_OFFSET(F) 0

/* Define this macro to nonzero value if the addresses of local variable slots
   are at negative offsets from the frame pointer.  */
#define FRAME_GROWS_DOWNWARD 1

/* Define this macro as a C expression that is nonzero for registers that are
   used by the epilogue or the return pattern.  The stack and frame
   pointer registers are already assumed to be used as needed.  */
#define EPILOGUE_USES(R) (R == V16_LR)

/* A C expression whose value is RTL representing the location of the
   incoming return address at the beginning of any function, before
   the prologue.  */
#define INCOMING_RETURN_ADDR_RTX gen_rtx_REG (Pmode, V16_LR)

/* Registers used for exception handling.  */
#define V16_EH_FIRST_REGNUM	(V16_R3)
#define V16_EH_LAST_REGNUM	(V16_R4)
#define V16_EH_TEMP_REGNUM	(V16_R5)

/* A C expression whose value is the Nth register number used for data by
   exception handlers, or INVALID_REGNUM if fewer than N registers are
   usable.  */
#define EH_RETURN_DATA_REGNO(N)					\
  ((N) <= (V16_EH_LAST_REGNUM - V16_EH_FIRST_REGNUM) ?	\
   V16_EH_FIRST_REGNUM + (N) : INVALID_REGNUM)

/* A C expression whose value is RTL representing a location in which to store
   a stack adjustment to be applied before function return. This is used to
   unwind the stack to an exception handler's call frame. It will be assigned
   zero on code paths that return normally.  */
#define EH_RETURN_STACKADJ_RTX	gen_rtx_REG (SImode, V16_EH_TEMP_REGNUM)

/* A C expression whose value is RTL representing a location in which to store
   the address of an exception handler to which we should return. It will not
   be assigned on code paths that return normally.  */
#define EH_RETURN_HANDLER_RTX						\
  gen_frame_mem (Pmode,							\
    plus_constant (Pmode, hard_frame_pointer_rtx, UNITS_PER_WORD))

/* Storage Layout */

#define BITS_BIG_ENDIAN 0
#define BYTES_BIG_ENDIAN 0
#define WORDS_BIG_ENDIAN 0

/* Alignment required for a function entry point, in bits.  */
#define FUNCTION_BOUNDARY 32

/* The alignment (log base 2) to put in front of LABEL.  */

#define LABEL_ALIGN(LABEL) v16_label_align (LABEL)

/* Define this macro as a C expression which is nonzero if accessing
   less than a word of memory (i.e. a `char' or a `short') is no
   faster than accessing a word of memory.  */
#define SLOW_BYTE_ACCESS 1

/* Number of storage units in a word; normally the size of a
   general-purpose register, a power of two from 1 or 8.  */
#define UNITS_PER_WORD 4

/* Define this macro to the minimum alignment enforced by hardware
   for the stack pointer on this machine.  The definition is a C
   expression for the desired alignment (measured in bits).  */
#define STACK_BOUNDARY 32

/* Normal alignment required for function parameters on the stack, in
   bits.  All stack parameters receive at least this much alignment
   regardless of data type.  */
#define PARM_BOUNDARY 32

/* Alignment of field after `int : 0' in a structure.  */
#define EMPTY_FIELD_BOUNDARY  32

/* No data type wants to be aligned rounder than this.  */
#define BIGGEST_ALIGNMENT 32

/* The best alignment to use in cases where we have a choice.  */
#define FASTEST_ALIGNMENT 32

/* Every structures size must be a multiple of 8 bits.  */
#define STRUCTURE_SIZE_BOUNDARY 8

/* Look at the fundamental type that is used for a bit-field and use
   that to impose alignment on the enclosing structure.
   struct s {int a:8}; should have same alignment as "int", not "char".  */
#define	PCC_BITFIELD_TYPE_MATTERS	1

/* Largest integer machine mode for structures.  If undefined, the default
   is GET_MODE_SIZE(DImode).  */
#define MAX_FIXED_MODE_SIZE 32

/* Make arrays of chars word-aligned for the same reasons.  */
#define DATA_ALIGNMENT(TYPE, ALIGN)		\
  (TREE_CODE (TYPE) == ARRAY_TYPE		\
   && TYPE_MODE (TREE_TYPE (TYPE)) == QImode	\
   && (ALIGN) < FASTEST_ALIGNMENT ? FASTEST_ALIGNMENT : (ALIGN))

/* Set this nonzero if move instructions will actually fail to work
   when given unaligned data.  */
#define STRICT_ALIGNMENT 1

/* Generating Code for Profiling */
#define FUNCTION_PROFILER(FILE,LABELNO) (abort (), 0)

/* Trampolines for Nested Functions.  */
#define TRAMPOLINE_SIZE (2 + 6 + 4 + 2 + 6)

/* Alignment required for trampolines, in bits.  */
#define TRAMPOLINE_ALIGNMENT 32

/* An alias for the machine mode for pointers.  */
#define Pmode         SImode

/* An alias for the machine mode used for memory references to
   functions being called, in `call' RTL expressions.  */
#define FUNCTION_MODE QImode

/* The register number of the stack pointer register, which must also
   be a fixed register according to `FIXED_REGISTERS'.  */
#define STACK_POINTER_REGNUM V16_SP

/* The register number of the frame pointer register, which is used to
   access automatic variables in the stack frame.  */
#define FRAME_POINTER_REGNUM V16_QFP

/* The register number of the arg pointer register, which is used to
   access the function's argument list.  */
#define ARG_POINTER_REGNUM V16_QAP

/* If necessary, we use R10 as the hard frame pointer register.  */
#define HARD_FRAME_POINTER_REGNUM V16_R10

#define ELIMINABLE_REGS					\
{{ ARG_POINTER_REGNUM,   STACK_POINTER_REGNUM},		\
 { ARG_POINTER_REGNUM,   HARD_FRAME_POINTER_REGNUM},	\
 { FRAME_POINTER_REGNUM, STACK_POINTER_REGNUM},		\
 { FRAME_POINTER_REGNUM, HARD_FRAME_POINTER_REGNUM}}

/* This macro returns the initial difference between the specified pair
   of registers.  */
#define INITIAL_ELIMINATION_OFFSET(FROM, TO, OFFSET)			\
  do {									\
    (OFFSET) = v16_initial_elimination_offset ((FROM), (TO));		\
  } while (0)

/* A C expression that is nonzero if REGNO is the number of a hard
   register in which function arguments are sometimes passed.  */
#define FUNCTION_ARG_REGNO_P(r) (r >= V16_R1 && r <= V16_R4)

/* A macro whose definition is the name of the class to which a valid
   base register must belong.  A base register is one used in an
   address which is the register value plus a displacement.  */
#define BASE_REG_CLASS GENERAL_REGS

#define INDEX_REG_CLASS NO_REGS

#define HARD_REGNO_OK_FOR_BASE_P(NUM) \
  ((unsigned) (NUM) < FIRST_PSEUDO_REGISTER \
   && (REGNO_REG_CLASS(NUM) == GENERAL_REGS \
       || (NUM) == HARD_FRAME_POINTER_REGNUM))

/* A C expression which is nonzero if register number NUM is suitable
   for use as a base register in operand addresses.  */
#define REGNO_OK_FOR_BASE_P(REGNO)	\
  v16_regno_ok_for_base_p (REGNO, true)

/* A C expression which is nonzero if register number NUM is suitable
   for use as an index register in operand addresses.  */
#define REGNO_OK_FOR_INDEX_P(REGNO) 0

/* A number, the maximum number of registers that can appear in a
   valid memory address.  */
#define MAX_REGS_PER_ADDRESS 1

/* The maximum number of bytes that a single instruction can move
   quickly between memory and registers or between two memory
   locations.  */
#define MOVE_MAX 4

/* A C expression that is nonzero if on this machine the number of
   bits actually used for the count of a shift operation is equal to
   the number of bits needed to represent the size of the object being
   shifted.  */
#define SHIFT_COUNT_TRUNCATED 1

/* All load operations zero extend.  */
#define LOAD_EXTEND_OP(MEM) ZERO_EXTEND

/* Define if operations between registers always perform the operation
   on the full register even if a narrower mode is specified.  */
#define WORD_REGISTER_OPERATIONS 1

/* Define this macro if it is advisable to hold scalars in registers
   in a wider mode than that declared by the program.  In such cases,
   the value is constrained to be within the bounds of the declared
   type, but kept valid in the wider mode.  The signedness of the
   extension may differ from that of the type.  */
#define PROMOTE_MODE(MODE, UNSIGNEDP, TYPE)	\
  if (GET_MODE_CLASS (MODE) == MODE_INT		\
      && GET_MODE_SIZE (MODE) < UNITS_PER_WORD)	\
    {						\
      if ((MODE) == SImode)			\
	(UNSIGNEDP) = 0;			\
      (MODE) = word_mode;			\
    }

/* An alias for a machine mode name.  This is the machine mode that
   elements of a jump-table should have.  */
#define CASE_VECTOR_MODE SImode

/* Define this so that jump tables go in same section as the current function,
   which could be text or it could be a user defined section.  */
#define JUMP_TABLES_IN_TEXT_SECTION 1

/* Run-time Target Specification */

#define TARGET_CPU_CPP_BUILTINS() \
  { \
    builtin_define ("__V16__");			\
    builtin_define ("__LITTLE_ENDIAN__");	\
    if (TARGET_MUL)			\
      builtin_define ("__V16_MUL__");		\
  }

/* Define these boolean macros to indicate whether or not your
   architecture has (un)conditional branches that can span all of
   memory.  */
#define HAS_LONG_COND_BRANCH false
#define HAS_LONG_UNCOND_BRANCH true

/* Define this macro if it is as good or better to call a constant
   function address than to call an address kept in a register.  */
#define NO_FUNCTION_CSE 1

#endif /* GCC_V16_H */
